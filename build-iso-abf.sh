#!/bin/sh

set -x
set -e
set -u
set +f

BUILD_ID="${BUILD_ID:-XXX}"
SLEEP="${SLEEP:-0}"

sleep "$SLEEP"

dnf distrosync -y

dnf install -y \
	coreutils \
	findutils \
	lsof \
	sed \
	tar \
	util-linux \
	/usr/bin/livecd-creator

mkdir -p /home/vagrant/results

livecd-creator --verbose \
	--compression-type=xz \
	--config=main.ks \
	--fslabel="ROSA_2021.1_SRV_Min_Test_${BUILD_ID}"

mv -v *.iso /home/vagrant/results/

repo --name=Main           --baseurl=http://abf-downloads.rosalinux.ru/rosa2021.1/repository/x86_64/main/release
repo --name=Main-Up        --baseurl=http://abf-downloads.rosalinux.ru/rosa2021.1/repository/x86_64/main/updates

repo --name=Contrib        --baseurl=http://abf-downloads.rosalinux.ru/rosa2021.1/repository/x86_64/contrib/release
repo --name=Contrib-Up     --baseurl=http://abf-downloads.rosalinux.ru/rosa2021.1/repository/x86_64/contrib/updates

repo --name=Non-Free       --baseurl=http://abf-downloads.rosalinux.ru/rosa2021.1/repository/x86_64/non-free/release
repo --name=Non-Free-Up    --baseurl=http://abf-downloads.rosalinux.ru/rosa2021.1/repository/x86_64/non-free/updates


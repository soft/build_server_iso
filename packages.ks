# Minimal system
# based on packages from rootfs
# https://github.com/mikhailnov/docker-rosa
basesystem-minimal
basesystem-mandatory
nano
rosa-repos
rosa-repos-contrib
rosa-repos-pkgprefs
branding-configs-fresh
task-kernel

# Locales
locales
locales-en
locales-ru

# Usefull stuff
chrony
openssh
openssh-server
openssh-clients
screen
rsync
wget
curl
curl-gost
openssl
libressl
ima-evm-utils

# monitoring utilities (maybe make a task-* package?)
htop
iotop
lm_sensors
nload
hw-probe
hdparm
traceroute
mtr
ncdu
pciutils
smartmontools
usbutils

# Automatic network configuration
# rpm -qa --queryformat="%{NAME}\n" | grep -i ^networkmanager
dhcp-client
networkmanager
#networkmanager-pptp
#networkmanager-vpnc
#networkmanager-l2tp
#networkmanager-openvpn

# Filesystem programs
btrfs-progs
dosfstools
e2fsprogs
exfat-utils
f2fs-tools
ntfs-3g
xfsprogs
lvm2

# Bootloaders
dracut
grub2
grub2-efi
grub2-theme-rosa
shim
syslinux
# syslinux & plymouth themes
Rosa-theme-EE

# Anaconda
anaconda
#task-anaconda
#metacity
#task-x11

# gnupg pulls pinentry
# pinentry-gui is pulled as a Recommended dependency of pinentry if x11-server-xorg is installed
# pinentry-qt5 will be chosen for pinentry-gui and will pull a tone of GUI packages
# They would be removed as orphans when removing Anaconda, but we do not need them in the ISO at all
-pinentry-gui

# tmp, for tests
#fcoe-utils
# XXX Anaconda crashes due to mpathconf utility being not found
# XXX dracut can't find pidof
#multipath-tools

auth --useshadow --enablemd5
selinux --disabled
# TODO: include firewalld
firewall --disabled
firstboot --enabled

# Enable/Disable services
services --enabled=systemd-resolved,NetworkManager,sshd

# partition
part / --size 5000 --fstype ext4

# configure Time Zone
timezone --utc Europe/Moscow

# configure language
lang en_US.UTF-8 --addsupport=ru_RU

# configure keyboard
keyboard en

# Ensure that Plymouth is disabled
# Start Anaconda installer (http://wiki.rosalab.ru/ru/index.php/Anaconda)
bootloader --append="plymouth.enable=0 systemd.unit=anaconda.target"

# repository
%include repobase.ks

%packages --nocore
%include packages.ks
%end

%post

passwd -d root

### Manually (re)generate initrds
# TODO: move its generation from %%post to %%postrrans in kernel
# packages, and then all needed dracut modules will be already
# installed during inird generation, and there will be no need
# to (re)generate initrds here.
# BUT dracut config by livecd-tools is partly broken
# https://github.com/livecd-tools/livecd-tools/issues/158
# We remove odd parts - modules that do not exist in this kernel.
# Another possible approach is making and using our own config from scratch.
C="${C:-/etc/dracut.conf.d/99-liveos.conf}"
find /boot -name 'vmlinuz-*' | sort -u | while read -r line ; do
	kver="$(echo "$line" | sed -e 's,^/boot/vmlinuz-,,g')"
	cp "${C}" "${C}.orig"
	to_find="$(cat "$C" | grep -E '^(filesystems\+=|add_drivers\+=)' | sed -e 's,",,g' | awk -F '+=' '{print $NF}' | tr ' ' '\n' | grep -v '=' | tr '\n' ' ' | sed -e 's,  , ,g')"
	not_exist_list=""
	for i in ${to_find} ; do
		if ! find "/lib/modules/${kver}" -name "${i}.ko*" | grep -q '.' ; then
			not_exist_list="${not_exist_list} ${i}"
		fi
	done
	sed -i -e 's,+=",+=" ,g' "$C"
	for i in ${not_exist_list} ; do
		sed -i -E -e "s,[[:blank:]]${i}[[:blank:]], ,g" "$C"
	done
	sed -i -e 's,  , ,g' "$C"
	diff -u "${C}.orig" "${C}" || :
	rm -f "${C}.orig"
	dracut -f "/boot/initrd-${kver}.img" "${kver}"
done
find /boot -name 'initrd-*' -print

%end

# debug
%post --nochroot
ls -la $LIVE_ROOT/isolinux/
cp -v "$(find "${INSTALL_ROOT}/boot" -name 'initrd-*.img' | sort -u | tail -n 1)" "${LIVE_ROOT}/isolinux/initrd0.img"
%end
